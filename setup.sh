#!/usr/bin/env bash

set option-name errexit

function exit_failure {
  echo "******************"
  echo "******************"
  echo "A FAILURE OCCURRED"
  echo "******************"
  echo "******************"
  exit 1
}

function exit_success {
  echo "*******"
  echo "Success"
  echo "*******"
  exit 0
}

function checkroot {
  if [[ $(id --user) == 0 ]]
  then
    echo "Do not run this as root!"
    exit_failure
  fi
}

function upgrade_existing {
  sudo apt-get --quiet update || exit_failure
  sudo apt-get --quiet --assume-yes --with-new-pkgs upgrade || exit_failure
}

function install_user_packages {
  sudo apt-get --quiet update || exit_failure
  sudo apt-get --quiet --assume-yes install \
    curl \
    gcc \
    git \
    jq \
    less \
    make \
    openssl \
    screen \
    tar \
    tree \
    vim \
    wget \
    zip \
    || exit_failure
}

function add_docker_key {

  docker_key="0EBFCD88"

  docker_fingerprint="9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88"

  tempdir=$(mktemp --directory)

  wget --quiet --directory-prefix="${tempdir}" --no-cache https://download.docker.com/linux/ubuntu/gpg || exit_failure

  sudo apt-key add "${tempdir}/gpg" || exit_failure

  sudo apt-key fingerprint "${docker_key}" >> "${tempdir}/fingerprint"

  grep --quiet "${docker_fingerprint}" "${tempdir}/fingerprint" || exit_failure

  rm --recursive "${tempdir}"

}

function install_docker {

  if [ -x "$(command -v docker)" ]
  then
    return 0
  fi

  add_docker_key || exit_failure

  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" || exit_failure

  sudo apt-get --quiet update || exit_failure
  sudo apt-get --quiet --assume-yes install \
    docker-ce \
    || exit_failure

  sudo groupadd docker
  sudo usermod --append --groups docker $USER || exit_failure
}

function setup_pyenv {

  if [ -x "$(command -v pyenv)" ]
  then
    return 0
  fi

  git clone https://github.com/pyenv/pyenv.git ~/.pyenv || exit_failure
  echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
  export PYENV_ROOT="$HOME/.pyenv"
  echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
  export PATH="$PYENV_ROOT/bin:$PATH"
  echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bashrc

}

function setup_pipsi {

  python_version=$1

  if [ -x "$(command -v pipsi)" ]
  then
    return 0
  fi

  tempdir=$(mktemp --directory)

  wget --quiet --directory-prefix="${tempdir}" --no-cache https://raw.githubusercontent.com/mitsuhiko/pipsi/master/get-pipsi.py || exit_failure

  ~/.pyenv/versions/"${python_version}"/bin/python "${tempdir}/get-pipsi.py" --src=git+https://github.com/mitsuhiko/pipsi.git#egg=pipsi || exit_failure

  rm --recursive "${tempdir}"

  echo '{"name": "pipsi", "version": "0.10.dev", "scripts": ["~/.local/bin/pipsi"]}' >> ~/.local/venvs/pipsi/package_info.json

  ~/.local/bin/pipsi install pipenv || exit_failure

}

function setup_python370 {

  python_version="3.7.0"

  if grep --quiet "${python_version}" <(pyenv versions)
  then
    setup_pipsi "${python_version}" || exit_failure
    return 0
  fi

  sudo apt-get --quiet update || exit_failure
  sudo apt-get --quiet --assume-yes --no-install-recommends install \
    libbz2-dev \
    libffi-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    libtinfo-dev \
    zlib1g-dev \
    || exit_failure

  ~/.pyenv/bin/pyenv install "${python_version}" || exit_failure
  ~/.pyenv/bin/pyenv global "${python_version}"

  sudo apt-get --quiet --assume-yes remove \
    libbz2-dev \
    libffi-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    libtinfo-dev \
    zlib1g-dev \
    || exit_failure

    setup_pipsi "${python_version}" || exit_failure
}

function setup_python366 {

  python_version="3.6.6"

  if grep --quiet "${python_version}" <(pyenv versions)
  then
    setup_pipsi "${python_version}" || exit_failure
    return 0
  fi

  sudo apt-get --quiet update || exit_failure
  sudo apt-get --quiet --assume-yes --no-install-recommends install \
    libbz2-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    libtinfo-dev \
    zlib1g-dev \
    || exit_failure

  ~/.pyenv/bin/pyenv install "${python_version}" || exit_failure

  sudo apt-get --quiet --assume-yes remove \
    libbz2-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    libtinfo-dev \
    zlib1g-dev \
    || exit_failure
}

function golang {

  if [ -x "$(command -v go)" ]
  then
    return 0
  fi

  tempdir=$(mktemp --directory)

  version="1.10.3"

  gzip="go${version}.linux-amd64.tar.gz"

  checkum="fa1b0e45d3b647c252f51f5e1204aba049cde4af177ef9f2181f43004f901035"

  wget --quiet --directory-prefix="${tempdir}" --no-cache "https://dl.google.com/go/${gzip}" || exit_failure

  echo "${checkum}  ${tempdir}/${gzip}" > "${tempdir}/${gzip}.sha256"

  sha256sum --check --strict "${tempdir}/${gzip}.sha256" || exit_failure

  sudo tar --directory /usr/local --extract --gzip --file "${tempdir}/${gzip}" || exit_failure

  rm --recursive "${tempdir}"

  echo 'export PATH="$PATH:/usr/local/go/bin"' >> ~/.bashrc

  mkdir "$HOME/go"
  mkdir "$HOME/go/src"
  mkdir "$HOME/go/bin"
  mkdir "$HOME/go/pkg"

  echo 'export GOPATH="$HOME/go"' >> ~/.bashrc

}

function install_jdk {
  sudo apt-get --quiet update || exit_failure

  sudo apt-get --quiet --assume-yes install \
    openjdk-8-jdk \
    || exit_failure
}

function add_sbt_source {

  if [ -f /etc/apt/sources.list.d/sbt.list ]
  then
    return 0
  fi

  echo "deb https://dl.bintray.com/sbt/debian /" >> sbt.list

  sudo mv sbt.list /etc/apt/sources.list.d/ || exit_failure

  sudo chmod 0664 /etc/apt/sources.list.d/sbt.list || exit_failure
}

function setup_sbt {

  if [ -x "$(command -v sbt)" ]
  then
    return 0
  fi

  install_jdk || exit_failure

  add_sbt_source || exit_failure

  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 || exit_failure

  sudo apt-get --quiet update || exit_failure

  sudo apt-get --quiet --assume-yes install \
    sbt \
    || exit_failure

}

function get_msft_key {
  currdir=$(pwd)
  tempdir=$(mktemp --directory)

  wget --quiet --directory-prefix="${tempdir}" --no-cache https://packages.microsoft.com/keys/microsoft.asc || exit_failure

  cd "${tempdir}" && gpg --dearmor "microsoft.asc" || exit_failure

  sudo apt-key add "${tempdir}/microsoft.asc.gpg" || exit_failure

  cd "${currdir}"

  rm --recursive "${tempdir}"
}

function add_vscode_source {

  if [ -f /etc/apt/sources.list.d/vscode.list ]
  then
    return 0
  fi

  echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" >> vscode.list

  sudo mv vscode.list /etc/apt/sources.list.d/ || exit_failure

  sudo chmod 0664 /etc/apt/sources.list.d/vscode.list || exit_failure
}

function install_vscode {

  if [ -x "$(command -v code)" ]
  then
    return 0
  fi

  get_msft_key || exit_failure
  add_vscode_source || exit_failure

  sudo apt-get --quiet update || exit_failure
  sudo apt-get --quiet --assume-yes install \
    code \
    || exit_failure
}

function add_google_source {

  if [ -f /etc/apt/sources.list.d/google-cloud-sdk.list ]
  then
    return 0
  fi

  echo "deb http://packages.cloud.google.com/apt cloud-sdk-bionic main" >> google-cloud-sdk.list

  sudo mv google-cloud-sdk.list /etc/apt/sources.list.d/ || exit_failure

  sudo chmod 0664 /etc/apt/sources.list.d/google-cloud-sdk.list || exit_failure

}

function add_google_keys {
  tempdir=$(mktemp --directory)

  wget --quiet --directory-prefix="${tempdir}" --no-cache https://packages.cloud.google.com/apt/doc/apt-key.gpg || exit_failure

  sudo apt-key add "${tempdir}/apt-key.gpg" || exit_failure

  rm --recursive "${tempdir}"
}

function install_gcloud_kubectl {

  add_google_keys || exit_failure
  add_google_source || exit_failure

  sudo apt-get --quiet update || exit_failure
  sudo apt-get --quiet --assume-yes install \
    google-cloud-sdk \
    kubectl \
    || exit_failure
}

function install_intellij {

  if [ -d /opt/idea* ]
  then
    return 0
  fi

  intellij_version=2018.2

  install_jdk || exit_failure

  currdir=$(pwd)
  tempdir=$(mktemp --directory)

  wget --quiet --directory-prefix="${tempdir}" --no-cache "https://download.jetbrains.com/idea/ideaIC-${intellij_version}-no-jdk.tar.gz" || exit_failure

  wget --quiet --directory-prefix="${tempdir}" --no-cache "https://download.jetbrains.com/idea/ideaIC-${intellij_version}-no-jdk.tar.gz.sha256" || exit_failure

  cd "${tempdir}" && sha256sum --check "ideaIC-${intellij_version}-no-jdk.tar.gz.sha256" || exit_failure

  cd "${tempdir}" && sudo tar --extract --gzip --file "ideaIC-${intellij_version}-no-jdk.tar.gz" --directory /opt/ || exit_failure

  cd "${currdir}"

  rm --recursive "${tempdir}"

  sudo apt-get --quiet update || exit_failure
  sudo apt-get --quiet --assume-yes install \
    libcanberra-gtk-module \
    || exit_failure

}

function add_azure_source {

  if [ -f /etc/apt/sources.list.d/azure-cli.list ]
  then
    return 0
  fi

  echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ bionic main" >> azure-cli.list

  sudo mv azure-cli.list /etc/apt/sources.list.d/ || exit_failure

  sudo chmod 0664 /etc/apt/sources.list.d/azure-cli.list || exit_failure

}

function install_azure {

  get_msft_key || exit_failure
  add_azure_source || exit_failure

  sudo apt-get --quiet update || exit_failure
  sudo apt-get --quiet --assume-yes install \
    azure-cli \
    || exit_failure
}

checkroot
upgrade_existing
install_user_packages
install_docker
setup_pyenv
setup_python370
setup_python366
golang
setup_sbt
install_vscode
install_gcloud_kubectl
install_intellij
install_azure
exit_success

